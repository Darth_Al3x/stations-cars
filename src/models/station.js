'use strict';

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const StationSchema = new Schema({
    name: {
        type: String,
        required:  [
            true,
            "Name is required."
        ],
        unique: true,
        min: 3
    },
    cars : [{
        type: Schema.Types.ObjectId,
        ref: "car"
    }]
});

const Station = mongoose.model('station', StationSchema);

module.exports = Station;