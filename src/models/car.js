'use strict';

const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CarSchema = new Schema({
    name: {
        type: String,
        required: [
            true,
            "Name is required."
        ],
        unique: true,
        min: 3
    },
    avaible: {
        type: Boolean,
        required: "Avaible is required"
    },
    station: {
        type: Schema.Types.ObjectId,
        ref: "station"
    }
});

const Car = mongoose.model('car', CarSchema);

module.exports = Car;