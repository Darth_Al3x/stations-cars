"use strict";

const router = require("express").Router();
const StationsService = require("../services/station");

router.route("/")
.post(StationsService.create)
    .get(StationsService.get);

router.route("/:stationId")
    .get(StationsService.read)
    .put(StationsService.update)
    .delete(StationsService.remove);

router.route("/cars")
    .get(StationsService.getWithCars);

router.route("/:stationId/cars")
    .get(StationsService.readWithCars)
    .post(StationsService.addNewCarToStation);

router.route("/:stationId/cars/:carId")
    .put(StationsService.addCarToStation)
    .put(StationsService.removeCarFromStation);

module.exports = router;