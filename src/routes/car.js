"use strict";

const router = require("express").Router();
const CarsService = require("../services/car");

router.route("/")
    .post(CarsService.create)
    .get(CarsService.get);
    
router.route("/:carId")
    .get(CarsService.read)
    .put(CarsService.update)
    .delete(CarsService.remove);

router.route("/station")
    .get(CarsService.getWithStation);

router.route("/:carId/station")
    .get(CarsService.readWithStation);

module.exports = router;