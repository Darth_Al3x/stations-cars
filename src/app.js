"use strict";

const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

const config = require("./config/config");

const Station = require("./models/station");
const Car = require("./models/car");

const stations = require("./routes/station");
const cars = require("./routes/car");

const app = express();
const port = config.port || 6666;

/**
 * Connection to MongoDB
 */
mongoose.connect(config.mongo_uri, { useNewUrlParser: true });

app
    .use(bodyParser.urlencoded({ extended: true }))
    .use(bodyParser.json())
    .use("/stations", stations)
    .use("/cars", cars)
    .listen(port);

module.exports = app;