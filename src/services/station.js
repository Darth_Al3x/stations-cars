"use strict";

const Station = require("../models/station");
const Car = require("../models/car");

module.exports = {
    
    get: async function(req, res, next) {
        const stations = await Station.find({});
        
        res.status(200).json({
            message: "Array containing all instances of Station.",
            success: true,
            data : stations
        });
    },
    
    getWithCars: async function(req, res, next) {
        const stations = await Station.find({}).populate('cars');

        res.status(200).json({
            message: "Array containing all instances of Station with Cars.",
            success: true,
            data : stations
        });
    },
    
    read: async function(req, res, next) {
        const { stationId } = req.params;
        const station = await Station.findById(stationId);

        res.status(200).json({
            message: "The instance of Station.",
            success: true,
            data : station
        });
    },
    
    readWithCars: async function(req, res, next) {
        const { stationId } = req.params;
        const station = await Station.findById(stationId).populate('cars');

        res.status(200).json({
            message: "The instance of Station with cars.",
            success: true,
            data : station
        });
    },    
    
    /**
     * Create a Station
     * @param {object} req 
     * @param {object} res 
     * @param {object} next 
     * @returns {Promise}
     */
    create: async function(req, res, next) {
        const newStation = new Station(req.body);
        const station = await newStation.save();

        res.status(201).json({
            message: "The instance of Station as been created.",
            success: true,
            data : station
        });
    },

    addCarToStation: async function(req, res, next) {
        const { stationId } = req.params;
        const { carId } = req.params;
        const car = await Car.findById(carId);
        const oldStation = await Station.findById(stationId).populate('cars');
        oldStation.cars.push(car);
        car.station = oldStation;        
        const station = await oldStation.save();
        await car.save();

        res.status(201).json({
            message: "The instance of Station as been updated, a Car was added on it.",
            success: true,
            data : station
        });
    },

    addNewCarToStation: async function(req, res, next) {
        const { stationId } = req.params;
        const newCar = new Car(req.body);
        const oldStation = await Station.findById(stationId).populate('cars');
        const car = await newCar.save();
        oldStation.cars.push(car);
        car.station = oldStation;        
        await oldStation.save();
        await car.save();
        const station = await Station.findById(stationId).populate('cars');

        res.status(201).json({
            message: "The instance of Station as been updated, a Car was added on it.",
            success: true,
            data : station
        });
    },
    
    update: async function(req, res, next) {
        const { stationId } = req.params;
        const newStation = new Station(req.body);
        await Station.findByIdAndUpdate(stationId, newStation);
        const station = await Station.findById(stationId).populate('cars');

        res.status(200).json({
            message: "The instance of Station as been updated.",
            success: true,
            data : station
        });
    },
    
    remove: async function(req, res, next) {
        const { stationId } = req.params;
        await Station.findByIdAndRemove(stationId);

        res.status(200).json({
            message: "The instance of Station as been removed.",
            success: true,
            data : null
        });
    },

    removeCarFromStation: async function(req, res, next) {
        const { stationId } = req.params;
        const { carId } = req.params;
        const station = await Station.findById(stationId).populate('cars');
        station.cars.remove({ _id: carId });        
        await station.save();

        res.status(201).json({
            message: "The instance of Station as been updated, a Car was deleted on it.",
            success: true,
            data : station
        });
    },
};
