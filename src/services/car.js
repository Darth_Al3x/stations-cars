"use strict";

const Car = require("../models/car");
const Station = require("../models/station");

module.exports = {
    
    get: async function(req, res, next) {
        const cars = await Car.find({});

        res.status(200).json({
            message: "Array containing all instances of Car.",
            success: true,
            data : cars
        });
    },
    
    getWithStation: async function(req, res, next) {
        const cars = await Car.find({}).populate('station');

        res.status(200).json({
            message: "Array containing all instances of Cars with Station.",
            success: true,
            data : cars
        });
    },
    
    read: async function(req, res, next) {
        const { carId } = req.params;
        const car = await Car.findById(carId);

        res.status(200).json({
            message: "The instance of Car.",
            success: true,
            data : car
        });
    },
    
    readWithStation: async function(req, res, next) {
        const { carId } = req.params;
        const car = await Car.findById(carId).populate('station');

        res.status(200).json({
            message: "The instance of Car with Station.",
            success: true,
            data : car
        });
    },
    
    create: async function(req, res, next) {
        const newCar = new Car(req.body);
        const car = await newCar.save();

        res.status(201).json({
            message: "The instance of Car as been created.",
            success: true,
            data : car
        });
    },
    
    update: async function(req, res, next) {
        const { carId } = req.params;
        const newCar = new Car(req.body);
        await Car.findByIdAndUpdate(carId, newCar);
        const car = await Car.findById(carId).populate('station');

        res.status(200).json({
            message: "The instance of Car as been updated.",
            success: true,
            data : car
        });
    },
    
    remove: async function(req, res, next) {
        const { carId } = req.params;
        const car = await Car.findById(carId).populate('station');
        const station = await Station.findById({ _id: car.station._id }).populate('cars');
        station.cars.remove({ _id: carId });
        await station.save();
        await Car.findByIdAndRemove(carId);

        res.status(200).json({
            message: "The instance of Car as been removed.",
            success: true,
            data : null
        });


    }
};
