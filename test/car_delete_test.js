'use strict';

const assert = require('assert');
const Car = require('./../src/models/car');

function assertHelper(statement, done) {
    statement
      .then(Car.find({}))
      .then(result => {
          assert(result === null);
      })
      .then(done)
      .catch(err => {
          done(new Error(err.message));
      });
}

describe('Deleting a Car', () => {
    let myCar;

    beforeEach(done => {
        myCar = new Car({ name: 'i30', avaible: true });
        myCar.save().then(done)
        .catch(err => {
            done(new Error(err.message));
        });
    });

    it('Removes a Car using its instance', done => {        
        assertHelper(myCar.remove().then(Car.findOne({ name: 'i35' })), done);
    });

    it('Removes multiple Cars', done => {        
        assertHelper(Car.remove({ name: 'i35' }).then(Car.findOne({ name: 'i35' })), done);
    });

    it('Removes a Car', done => {
        assertHelper(Car.findOneAndRemove({ name: 'i35' }).then(Car.findOne({ name: 'i35' })), done);        
    });

    it('Removes a Car using id', done => {
        assertHelper(Car.findIdAndRemove(myCar._id).then(Car.findOne({ name: 'i35' })), done);        
    });
});