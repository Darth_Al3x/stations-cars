'use strict';

const assert = require('assert');
const Station = require('./../src/models/station');

let myStation;

beforeEach(done => {
    myStation = new Station({  name: 'Tours' });
    myStation.save().then(done)
        .catch(err => {
            done(new Error(err.message));
        });
});

describe('Reading a Station', () => {
    it('Find the Station with the name of Station', done => {
        Station.findOne({ name: 'Tours' })
            .then(result => {
                assert(result.name === 'Tours'); 
            })
            .then(done)
            .catch(err => {
                done(new Error(err.message));
            });
    })
})