'use strict';

const assert = require('assert');
const Station = require('./../src/models/station');

describe('Updating a Station', () => {

    let myStation;

    beforeEach(done => {
        myStation = new Station({ name: 'Tours' });
        myStation.save().then(done)
        .catch(err => {
            done(new Error(err.message));
        });
    });

    function assertHelper(statement, done) {
        statement
        .then(Station.find({}))
        .then(stations => {
            assert(stations.length === 1);
            assert(stations[0].name === 'Tours');
        })
        .then(done)
        .catch(err => {
            done(new Error(err.message));
        });
    }

    it('Sets and saves Station using an instance', done => {
        myStation.set('name', 'Tours');
        assertHelper(myStation.save(), done);
    });

    it('Update Station using instance', done => {
        assertHelper(myStation.update({ name: 'Tours' }), done);
    });

    it('Update all matching Stations using model', done => {
        assertHelper(Station.update({ name: 'Angers' }, { name: 'Tours' }), done);
    });

    it('Update one Station using model', done => {
        assertHelper(Station.findOneAndUpdate({ name: 'Angers' }, { name: 'Tours' }), done);
    });

    it('Update one Station with id using model', done => {
        assertHelper(Station.findByIdAndUpdate(myStation._id, { name: 'Tours' }), done);
    });
});