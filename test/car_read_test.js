'use strict';

const assert = require('assert');
const Car = require('./../src/models/car');

let car;

beforeEach(done => {
    car = new Car({ name: 'i30', avaible: true });
    car.save().then(done)
        .catch(err => {
            done(new Error(err.message));
        });
});

describe('Reading a Car', () => {
    it('Find the Car with the name of Car', done => {
        Car.findOne({ name: 'i30' })
            .then(result => {
                assert(result.name === 'i30');
            })
            .then(done)
            .catch(err => {
                done(new Error(err.message));
            });
    })
})