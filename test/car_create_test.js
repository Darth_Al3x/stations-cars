'use strict';

const assert = require('assert');
const Car = require('./../src/models/car');

describe('Creating Cars', () => {

    it('Create a Car', done => {
        const car = new Car({ name: 'i30', avaible: true });
        car.save()
            .then(() => {
                assert(!car.isNew);
            })
            .then(done)
            .catch(err => {
                done(new Error(err.message));
            });
    });

});