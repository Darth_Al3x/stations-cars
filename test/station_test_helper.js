'use strict';

const mongoose = require("mongoose");
const config = require("./../src/config/config");

mongoose.Promise = global.Promise;
mongoose.connect(config.mongo_uri, { useNewUrlParser: true }); 
mongoose.connection
    .once("open", () => {console.log("Connected!")})
    .on("error", error => {
        console.warn("Error : ",error);
    });

beforeEach(done => {
    mongoose.connection.collections["stations"].drop(() => { done(); }); 
});