'use strict';

const assert = require('assert');
const Station = require('./../src/models/station');

describe('Creating Stations', () => {

    it('Create a Station', done => {
        const station = new Station({ name: 'Tours' });
        station.save()
            .then(() => {
                assert(!station.isNew);
            })
            .then(done)
            .catch(err => {
                done(new Error(err.message));
            });
    });

});