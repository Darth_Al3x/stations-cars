
'use strict';

const assert = require('assert');
const Car = require('./../src/models/car');

describe('Updating a Car', () => {

    let myCar;

    beforeEach(done => {
        myCar = new Car({ name: 'i30', avaible: true });
        myCar.save().then(done)
    });

    function assertHelper(statement, done) {
        statement
        .then(Car.find({}))
        .then(cars => {
            assert(cars.length === 1);
            assert(cars[0].name === 'i30');
        })
        .then(done)
    }

    it('Sets and saves Car using an instance', done => {
        myCar.set('name', 'i30');
        assertHelper(myCar.save(), done);
    });

    it('Update Car using instance', done => {
        assertHelper(myCar.update({ name: 'i30' }), done);
    });

    it('Update all matching Cars using model', done => {
        assertHelper(Car.update({ name: 'i35' }, { name: 'i30' }), done);
    });

    it('Update one Car using model', done => {
        assertHelper(Car.findOneAndUpdate({ name: 'i35' }, { name: 'i30' }), done);
    });

    it('Update one Car with id using model', done => {
        assertHelper(Car.findByIdAndUpdate(myCar._id, { name: 'i30' }), done);
    });
});