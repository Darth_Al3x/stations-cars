'use strict';

const assert = require('assert');
const Station = require('./../src/models/station');

function assertHelper(statement, done) {
    statement
        .then(Station.find({}))
        .then(result => {
            assert(result === null);
        })
        .then(done)
        .catch(err => {
            done(new Error(err.message));
        });
}

describe('Deleting a Station', () => {
    let myStation;

    beforeEach(done => {
        myStation = new Station({ name: 'Tours' });
        myStation.save().then(done)
        .catch(err => {
            done(new Error(err.message));
        });
    });

    it('Removes a Station using its instance', done => {        
        assertHelper(myStation.remove().then(Station.findOne({ name: 'Tours' })), done);
    });

    it('Removes multiple Stations', done => {        
        assertHelper(Station.remove({ name: 'Tours' }).then(Station.findOne({ name: 'Tours' })), done);
    });

    it('Removes a Station', done => {
        assertHelper(Station.findOneAndRemove({ name: 'Tours' }).then(Station.findOne({ name: 'Tours' })), done);        
    });

    it('Removes a Station using id', done => {
        assertHelper(Station.findIdAndRemove(myStation._id).then(Station.findOne({ name: 'Tours' })), done);        
    });
});