# Stations&Car

This is a micro-service that allows to set up a REST API using the technologies below :

* NodeJs
* ExpressJs
* Mongoose
* Mocha
* Body Parser

## Initialization
---
 * 1 : Into "src" create new file with name is config.js and paste content of config.exemple.js then add your properties !!!
```promt
cd config && touch config.js
```

 * 2 To initialize the project, use the following command into your terminal.
```prompt
npm i
```

 * 3 To start the project, use this other command. (Always into your terminal ;p)
```prompt
npm start
```

 * 4 To test the project, use this other command. (Always into your terminal ;p)
```prompt
npm test
```

## Router
---

## Stations : 
Get all Stations
```http
GET /stations
```
Create a Station
```http
POST /stations
```
Read a Stations
```http
GET /stations/:stationId
```
Update a Station
```http
PUT /stations/:stationId
```
Delete a Station
```http
DELETE /stations/:stationId
```
Get all Stations with Cars
```http
GET /stations/cars
```
Read a Station with Cars
```http
GET /stations/:stationId/cars
```
Add New Car to Station
```http
POST /stations/:stationId/cars
```
Add a existing car to Station
```http
POST /stations/:stationId/cars/:carId
```
Remove a Car from a Station
```http
PUT /stations/:stationId/cars/:carId
```

## Cars : 
Get all Cars
```http
GET /cars
```
Create a Car
```http
POST /cars
```
Read a Car
```http
GET /cars/:carId
```
Update a Car
```http
PUT /cars/:carId
```
Delete a Car
```http
DELETE /cars/:carId
```
Get all Car with Station
```http
GET /cars/station
```
Read a Car with Station
```http
GET /cars/:carId/station
```

## Responses
---

## Stations : 

```json
{
    name: {
        type: String,
        required: "Name is required",
        unique: true,
        min: 3
    },
    cars : [{
        type: Schema.Types.ObjectId,
        ref: "car"
    }]
}
```


## Cars : 

```json
{
    name: {
        type: String,
        required: "Name is required",
        unique: true,
        min: 3
    },
    avaible: {
        type: Boolean,
        required: "Avaible is required"
    },
    station: {
        type: Schema.Types.ObjectId,
        ref: "station"
    }
}
```
